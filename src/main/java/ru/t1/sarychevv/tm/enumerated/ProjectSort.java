package ru.t1.sarychevv.tm.enumerated;

import ru.t1.sarychevv.tm.comparator.CreatedComparator;
import ru.t1.sarychevv.tm.comparator.NameComparator;
import ru.t1.sarychevv.tm.comparator.StatusComparator;
import ru.t1.sarychevv.tm.model.Project;

import java.util.Comparator;

public enum ProjectSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    private final String displayName;

    private final Comparator<Project> comparator;

    ProjectSort(final String displayName, final Comparator<Project> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static ProjectSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator<Project> getComparator() {
        return comparator;
    }

}
