package ru.t1.sarychevv.tm.service;

import ru.t1.sarychevv.tm.api.repository.IProjectRepository;
import ru.t1.sarychevv.tm.api.service.IProjectService;
import ru.t1.sarychevv.tm.exception.field.DescriptionEmptyException;
import ru.t1.sarychevv.tm.exception.field.NameEmptyException;
import ru.t1.sarychevv.tm.exception.field.UserIdEmptyException;
import ru.t1.sarychevv.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    public void create(final String userId, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        if (userId == null) throw new UserIdEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        add(project);
    }

    public void create(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null) throw new UserIdEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        add(project);
    }

}
