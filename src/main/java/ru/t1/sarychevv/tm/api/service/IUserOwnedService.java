package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.api.repository.IUserOwnedRepository;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    void create(String userId, String name, String description);

    M updateById(String userId, String id, String name, String description);

    M updateByIndex(String userId, Integer index, String name, String description);

    M changeStatusById(String userId, String id, Status status);

    M changeStatusByIndex(String userId, Integer index, Status status);

}

