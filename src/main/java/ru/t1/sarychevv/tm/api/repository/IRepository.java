package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    void removeAll();

    M removeOne(M model);

    M removeOneById(String id);

    M removeOneByIndex(Integer index);

    Integer getSize();

    Boolean existsById(String id);

}
