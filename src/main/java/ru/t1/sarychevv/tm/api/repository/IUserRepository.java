package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
