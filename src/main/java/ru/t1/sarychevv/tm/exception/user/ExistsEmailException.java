package ru.t1.sarychevv.tm.exception.user;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! User with this email already exists...");
    }

}
