package ru.t1.sarychevv.tm.command.user;

import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "User lock";
    }

    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
