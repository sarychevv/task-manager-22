package ru.t1.sarychevv.tm.command.task;

import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.model.Task;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Show list tasks.";
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        final String sortType = TerminalUtil.nextLine();
        final TaskSort sort = TaskSort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> tasks;
        if (sort == null) tasks = getTaskService().findAll(userId);
        else tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 0;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getId() + ": " + task.getName() +
                    " " + task.getDescription() + " " + Status.toName(task.getStatus()));
            index++;
        }
    }

}
